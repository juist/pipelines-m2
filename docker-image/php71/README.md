# Updating Docker Hub

1. Change and build the image locally (go to this directory in your shell and run `docker build .`)
2. Run `docker images` and find the image you just built (hint: usually at the top of the list)
3. Run `docker tag <my-image-tag> getnoticed/deployerm2`
4. Run `docker push getnoticed/deployerm2`

# Please note - authorization
In order to update the repository (`getnoticed/deployerm2`) you must be authorized for the team first.
Ask one of your colleagues with access to provide authorization.