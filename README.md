Magento 2 - BitBucket Pipelines Configuration
-----

# UPGRADING - IMPORTANT

# 4.x - 5.x

When upgrading from any 4.x version to any 5.x version, please read this section carefully.

The structure of the deployer system was changed, we migrated from the Deployer default Magento 2 recipe
to our own recipe, to prevent problems with future upgrades.

## Upgrade steps

1. Copy the contents of `deploy.php.dist` to `deploy.php`
2. If you have customizations in `deploy-custom.php`, copy them over to `deploy.php` now.
3. Check if the `hosts.yml` file is still correct.
4. If necessary, copy `themes.json.dist` to `themes.json` and change the values to reflect your project.
5. Remove all dist files and any other unused files
6. Perform a composer install and commit all changes from the .deploy folder + bitbucket-pipelines.yml.

# Installation

1. Add the code under "Installation > composer.json code" to your projects `composer.json` file.
2. Run `composer require getnoticed/pipelines-m2` in your project.
3. Copy the dist files in the `.deploy` folder and change them to reflect the projects values.
4. Enable **BitBucket Pipelines** in the project (under *BitBucket Project > Settings > Pipelines > Settings*)
5. Generate an SSH key in *BitBucket project > Settings > Pipelines > SSH keys*.

## composer.json code

```json
{
  "extra": {
    "copy-file": {
      "vendor/getnoticed/pipelines-m2/dist/": "."
    }
  },
  "scripts": {
    "post-install-cmd": ["SlowProg\\CopyFile\\ScriptHandler::copy"],
    "post-update-cmd": ["SlowProg\\CopyFile\\ScriptHandler::copy"]
  }
}
```

# Preparing a NEW environment

1. On the server, create the `~/deploy/shared` directory.
2. In the shared directory, create `app/etc/env.php` and place a valid Magento 2 env file here. See the section **Creating the Magento 2 env file** for more information.
3. Upload a valid database to the database defined in `app/etc/env.php`. Remember to change the base URL's and other necessary settings.
4. Go to the `~/domains/domain.tld` directory.
5. Remove `public_html` and `private_html` (check if there is no actual content in these! If there is, you should have followed the **Preparing an EXISTING environment** steps)
6. Create two symlinks to `~/deploy/current/pub/` named `public_html` and `private_html`
7. For AC/PROD environments, it might be necessary to copy media or any additional (shared) files. If so, do this now.
8. Make sure to chmod the autherized_keys files to 600
9. Trigger a pipeline through any method you like and deploy the environment.

## Creating the Magento 2 env file

1. Do you have an existing DV-environment? Excellent! Use the `app/etc/env.php` file from this environment and continue to step 3.
2. No existing file? Bummer, use the `app/etc/env.php` file from your location environment and continue to step 3.
3. Change the database credentials to those of the new environment you're setting up.
4. Set the application mode to **production** if this is not yet the case.

# Preparing an EXISTING environment

* Shared directory: `~/deploy/shared`
* Domains directory: `~/domains/domain.tld`
* Currently active directory: the root of the currently deployed version, e.g. `~/domains/domain.tld/root`

1. On the server, create the `~/deploy/shared` directory.
2. Copy the `app/etc/env.php` file from the currently active directory to the shared directory, while maintaining it's full path (e.g. copy `~/domains/domain.tld/root/app/etc/env.php` to `~/deploy/shared/app/etc/env.php`)
3. Do the same for `var/.maintenance.ip`, `var/log`, `var/backups` and `pub/media`. If your project configures any additional shared files or dirs (see `.deploy/deploy-custom.php`) copy these too.
4. Temporarily copy the currently active directory to somewhere where you can reach it again later (in case something goes wrong)
5. Create two symlinks to `~/deploy/current/pub/` named `public_html` and `private_html`
6. Trigger a pipeline through any method you like and deploy the environment.
