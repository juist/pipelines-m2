<?php

function recursive_search($directoryPath, $pattern)
{
    $directoryIterator = new \RecursiveDirectoryIterator($directoryPath);
    $iterator = new \RecursiveIteratorIterator($directoryIterator);
    $filesFromRegex = new \RegexIterator($iterator, $pattern, \RegexIterator::GET_MATCH);

    return array_map(
        function (array $match) {
            return $match[0];
        },
        \iterator_to_array($filesFromRegex)
    );
}