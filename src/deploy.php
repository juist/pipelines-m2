<?php
/**
 * Get.Noticed Deployer bootstrap file.
 *
 * This file is the main point included by all projects that are deploying using Deployer PHP.
 * It contains all the necessary basics to deploy a Magento 2 project using Deployer PHP.
 *
 * For customization, see the README.md file of this module.
 */

namespace Deployer;

define('GETNOTICED_DEPLOYER_VENDOR_DIRECTORY', __DIR__);
define('GETNOTICED_DEPLOY_DIRECTORY', realpath(__DIR__ . '/../../../..') . DIRECTORY_SEPARATOR . '.deploy');

## ------------------------------------------------------------------------
## Basics: include Deployers common recipe (necessary base for all deploys)

require_once 'recipe/common.php';

## ------------------------------------------------------------------------
## Set php version

set('bin/php', function () {
    return '/usr/local/php74/bin/php';
});


## ------------------------------------------------------------------------
## Bootstrap - load functions, configurations, tasks, etc

array_map(
    function (string $functionFile) {
        require_once $functionFile;
    },
    (array)glob(GETNOTICED_DEPLOYER_VENDOR_DIRECTORY . '/functions/*.php')
);

require GETNOTICED_DEPLOYER_VENDOR_DIRECTORY . '/config.php';
require GETNOTICED_DEPLOYER_VENDOR_DIRECTORY . '/binaries.php';

array_map(
    function (string $taskFile) {
        require_once $taskFile;
    },
    recursive_search(GETNOTICED_DEPLOYER_VENDOR_DIRECTORY . '/tasks', '/^(.*).php$/')
);
