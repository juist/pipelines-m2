<?php

namespace Deployer;

// Set timeout to 30 minutes, to prevent timeouts
set('default_timeout', 30 * 60);

// File and directory configuration
set(
    'shared_files',
    [
        'app/etc/env.php',
        'var/.maintenance.ip',
        'var/.test-environment.ip'
    ]
);
set(
    'shared_dirs',
    [
        'var/log',
        'var/backups',
        'var/imex',
        'var/import',
        'var/export',
        'pub/sitemaps',
        'pub/media',
        'pub/.well-known'
    ]
);
set(
    'writable_dirs',
    [
        'var',
        'pub/static',
        'pub/media',
    ]
);
set(
    'clear_paths',
    [
        'var/generation/*',
        'var/cache/*',
    ]
);

// Application configuration

define('APPLICATION', getenv('BITBUCKET_REPO_SLUG'));
define('REPOSITORY_URL', sprintf('git@bitbucket.org:Juist/%s.git', APPLICATION));

set('application', APPLICATION);
set('repository', REPOSITORY_URL);

// Change writable settings so it works on all environments (Virtio, Byte, etc)

set('writable_use_sudo', false);
set('writable_mode', 'chmod');

// Load configured hosts

inventory('hosts.yml');
