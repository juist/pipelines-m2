<?php

namespace Deployer;

task(
    'magento:setup:upgrade',
    function () {
        run("{{bin/magento}} setup:upgrade --keep-generated");
    }
)->desc('Magento - setup:upgrade');