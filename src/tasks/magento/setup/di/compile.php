<?php

namespace Deployer;

task(
    'magento:setup:di:compile',
    function () {
        run("{{bin/magento}} setup:di:compile");
        run("cd {{release_path}} && {{bin/composer}} dump-autoload -o");
    }
)->desc('Magento - setup:di:compile');