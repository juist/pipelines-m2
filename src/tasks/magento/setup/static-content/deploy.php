<?php

namespace Deployer;

$themeConfig = \file_get_contents(GETNOTICED_DEPLOY_DIRECTORY . DIRECTORY_SEPARATOR . 'themes.json');
$themeConfig = \json_decode($themeConfig, true);

$deployLanguages = getenv('M2_DEPLOY_LANGUAGES') ?: 'en_US';
$parallelJobs = getenv('M2_DEPLOY_PARALLEL_JOBS') ?: 2;

task(
    'magento:setup:static-content:deploy',
    function () use ($themeConfig, $deployLanguages, $parallelJobs) {
        // Check if it's a frontools theme
        $isFrontoolsInstalled = test("[ -d {{release_path}}/vendor/snowdog/frontools ]");
        $clearViewPreprocessedDirectoryCmd = "rm -rf {{release_path}}/var/view_preprocessed";

        if ($isFrontoolsInstalled) {
            // Run Yarn to install Frontools
            run("cd {{release_path}}/vendor/snowdog/frontools && {{bin/yarn}}");

            // Clean the var/view_preprocessed directory, because it sometimes contains directories/files
            // that break Magento's setup static content deploy proces.
            run($clearViewPreprocessedDirectoryCmd);
        }

        if (is_array($themeConfig) && array_key_exists('deploy', $themeConfig)) {
            if ($isFrontoolsInstalled && array_key_exists('frontools-themes', $themeConfig)
                && is_array($themeConfig['frontools-themes']) && !empty($themeConfig['frontools-themes'])) {
                $frontoolsThemes = $themeConfig['frontools-themes'];
            } else {
                $frontoolsThemes = [];
            }

            foreach ($themeConfig['deploy'] as $area => $themes) {
                foreach ($themes as $theme => $languages) {
                    foreach ($languages as $language) {
                        run(
                            sprintf(
                                '{{bin/magento}} setup:static-content:deploy -j %1$d -a %2$s -t %3$s %4$s',
                                (int)$parallelJobs,
                                $area,
                                $theme,
                                $language
                            )
                        );
                    }

                    if ($isFrontoolsInstalled && array_key_exists($theme, $frontoolsThemes)) {
                        $buildTheme = $frontoolsThemes[$theme];
                        $stylesCmd = sprintf(
                            "cd {{release_path}}/vendor/snowdog/frontools" .
                            "&& {{bin/gulp}} styles --prod --disableMaps --ci --theme %s",
                            $buildTheme
                        );

                        // Create styles in pub directory
                        run($stylesCmd);

                        // Clean the var/view_preprocessed directory, because it sometimes contains directories/files
                        // that break Magento's setup static content deploy proces.
                        run($clearViewPreprocessedDirectoryCmd);
                    }
                }
            }
        } else {
            run(
                sprintf(
                    '{{bin/magento}} setup:static-content:deploy -j %2$d %1$s',
                    $deployLanguages,
                    (int)$parallelJobs
                )
            );
        }
    }
)->desc('Magento - setup:static-content:deploy');