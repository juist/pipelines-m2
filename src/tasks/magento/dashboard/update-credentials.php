<?php

namespace Deployer;

task(
    'magento:dashboard:update-credentials',
    function () {
        if (test("[ -d {{deploy_path}}/current/vendor/getnoticed/module-magento-dashboard ]")) {
            run("{{bin/php}} {{deploy_path}}/current/bin/magento dashboard:update-credentials");
        }
    }
)->desc('Magento - dashboard:update-credentials')->onStage(['production']);

after('deploy:symlink', 'magento:dashboard:update-credentials');