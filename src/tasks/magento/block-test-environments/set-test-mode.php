<?php

namespace Deployer;

task(
    'gn:block-test-environments:enable-test-mode',
    function () {
        $testMode = getenv('TEST_MODE');

        if ($testMode === 'DISABLE') {
            return;
        }

        if (test("[ -d {{deploy_path}}/current/vendor/getnoticed/module-block-test-environments ]")) {
            run("{{bin/php}} {{deploy_path}}/current/bin/magento bte:test-mode:enable");
        }
    }
)->desc('Magento - gn:block-test-environments:enable-test-mode')->onStage(['test', 'staging']);

after('deploy:symlink', 'gn:block-test-environments:enable-test-mode');