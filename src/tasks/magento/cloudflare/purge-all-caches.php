<?php

namespace Deployer;

task(
    'gn:cf-api:zones:purge-all',
    function () {
        if (test("[ -d {{deploy_path}}/current/vendor/getnoticed/module-cloudflare-mini ]")) {
            run("{{bin/php}} {{deploy_path}}/current/bin/magento gn:cf-api:zones:purge-all");
        }
    }
)->desc('Magento - gn:cf-api:zones:purge-all')->onStage(['production']);

after('deploy:symlink', 'gn:cf-api:zones:purge-all');