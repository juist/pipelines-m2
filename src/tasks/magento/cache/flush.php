<?php

namespace Deployer;

task(
    'magento:cache:flush',
    function () {
        run("{{bin/magento}} cache:flush");
    }
)->desc('Magento - cache:flush');