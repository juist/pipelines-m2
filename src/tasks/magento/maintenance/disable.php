<?php

namespace Deployer;

task(
    'magento:maintenance:disable',
    function () {
        run("if [ -d $(echo {{deploy_path}}/current) ]; then {{bin/magento}} maintenance:disable; fi");
    }
)->desc('Magento - maintenance:disable');