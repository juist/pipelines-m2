<?php

namespace Deployer;

task(
    'magento:maintenance:enable',
    function () {
        run("if [ -d $(echo {{deploy_path}}/current) ]; then {{bin/magento}} maintenance:enable; fi");
    }
)->desc('Magento - maintenance:enable');