<?php

namespace Deployer;

task(
    'deploy:magento',
    [
        'magento:setup:upgrade',
        'magento:setup:di:compile',
        'magento:setup:static-content:deploy',
        'magento:cache:flush',
    ]
)->desc('Magento 2 specific deployment commands');
