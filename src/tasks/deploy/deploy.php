<?php

namespace Deployer;

task(
    'deploy',
    [
        'deploy:info',
        'deploy:prepare',
        // Locking disabled on purpose
        // 'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        'deploy:shared',
        'deploy:writable',
        'deploy:vendors',
        'deploy:clear_paths',
        'deploy:magento',
        'deploy:symlink',
        // Locking disabled on purpose
        // 'deploy:unlock',
        'cleanup',
        'success'
    ]
)->desc('Deploy your application');

// To prevent the production environment from getting bricked if a deploy fails during
// setup:upgrade, always disable the maintenance mode after a failed deploy.
after('deploy:failed', 'magento:maintenance:disable');