<?php

namespace Deployer;

task(
    'composer:set-packagist-token',
    function () {
        $token = getenv('PACKAGIST_TOKEN');

        if (empty($token)) {
            throw new \InvalidArgumentException(
                'No token specified - please set the $PACKAGIST_TOKEN variable in BitBucket Pipelines repository variables.'
            );
        }

        writeln(sprintf('<info>Using Packagist.com with token:</info> %s', $token));
        run(sprintf('{{bin/composer}} config --global --auth http-basic.repo.packagist.com token %s', $token));
    }
);

before('deploy:vendors', 'composer:set-packagist-token');