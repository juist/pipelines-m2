<?php

namespace Deployer;

set(
    'bin/magento',
    function () {
        return sprintf(
            '%s {{release_path}}/bin/magento',
            locateBinaryPath('php')
        );
    }
);

set(
    'bin/mv',
    function () {
        return locateBinaryPath('mv');
    }
);

set(
    'bin/npm',
    function () {
        return locateBinaryPath('npm');
    }
);

set(
    'bin/gulp',
    function () {
        return locateBinaryPath('gulp');
    }
);

set(
    'bin/yarn',
    function () {
        return locateBinaryPath('yarn');
    }
);